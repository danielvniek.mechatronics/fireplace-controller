#include <Servo.h>

Servo esc;
int escVal;


void setup() {
esc.attach(9, 1000, 2000);
}

void loop() {
  escVal = analogRead(A0);
  escVal = map(escVal, 0, 1023, 0, 180);
  esc.write(escVal);    
}
