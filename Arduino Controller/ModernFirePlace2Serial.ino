#include <SoftwareSerial.h>
#include <Servo.h>
#include "max6675.h"
#include <Math.h>
#define TxD 11
#define RxD 10
SoftwareSerial bluetoothSerial(RxD,TxD);
char incoming[4];
char n;
int automation = 0;

//esc init start
Servo esc;

int escGoal=30;
int MAXSPEED=110;
int recordMaxSpeed = 0;
const int pwmPin = 12;
//esc init end
//thermoK init start
const int soPin=4;
const int csPin=5;
const int sckPin=6;
MAX6675 thermocouple(sckPin,csPin,soPin);
int stoveRecordEnable = 0;
float Tk;
//thermoK init end
//stepper init start
const int dirPin = 7;
const int stepPin = 8;
const int leftLimit = 2;
const int rightLimit = 3;
int tick = 0;

int mod = 1;
int stepsPerStroke = 0;
int stepGoal = 0;
int stepCurrent = 0;//change back to 0
int stepState = 0;
int dirState = 0;
//stepper init end
//thermistors init start
int roomRecordEnable=0;
int ThermistorPin1 = 0;
int Vo1;
float R1_1 = 10000;
float logR2_1, R2_1, T1;
float c1_1 = 1.009249522e-03, c2_1 = 2.378405444e-04, c3_1 = 2.019202697e-07;

int ThermistorPin2 = 1;
int Vo2;
float R1_2 = 10000;
float logR2_2, R2_2, T2;
float c1_2 = 1.009249522e-03, c2_2 = 2.378405444e-04, c3_2 = 2.019202697e-07;

float T1avg=0;
float T2avg=0;
float Tavg=0;
//thermistors init end

int dataLoggerUpdate = 0;
float t = 0.00;
float O = 50.54;
float V = 15.23;
float checkSerial = 0.00;
int appUpdateEnable = 0;

//PID constants
double kp = 16;//15//32
double ki = 1;//5//2

unsigned long currentTime=0;
unsigned long previousTime=0;
double elapsedTime;
double outNow;
double outPrev;
double outPrevPrev;
double errorNow;
double errorPrev;
double dataBlockA;
double dataBlockB;

int tempRef = 25;
int updateFan=0;

int c=0;
int w=0;
int tooCold=0;
int tooWarm=0;
int perfectTemp=0;
int coldSent,warmSent;

void setup() {
  bluetoothSerial.begin(9600);
  Serial.begin(9600);
  esc.attach(pwmPin, 1000, 2000);
  pinMode(dirPin, OUTPUT);
  pinMode(stepPin, OUTPUT);
  pinMode(leftLimit, INPUT);
  pinMode(rightLimit, INPUT);  
 //stepperRangeFinder(); //to find amount of steps from left side to right side: code will not continue until limit pins have gone high in the right order 
 timerSetup();
}

ISR(TIMER2_COMPA_vect){  //stepping and temp recording timer at 8kHz
  tick++; 
  mod = tick%100;
    
  if(mod == 0){
    if (stepCurrent > stepGoal){
             dirState = 0;
             stepState = 1;
             stepCurrent--; 
    }
   if (stepCurrent < stepGoal){
             dirState = 1;
             stepState = 1;
             stepCurrent++; 
   }
  }
   if(mod ==50){
    stepState = 0;          
   }
   if (tick==4000){
    tick=0;
   }
 
   if (tick%1000==0){  // take room temp samples at 8Hz:highest rate possible that allows all measurements to be taken without data corruption or timer delays
    roomRecordEnable = 1;
   }
   if (tick%2000==0){
     updateFan = 1;
   }
     
   if ((tick == 500)||(tick==2500)){// not on thousands so that room record and gui update never on same instant. Update 4 times a second
    dataLoggerUpdate = 1;
    t= t+0.25;
   }
   if (tick ==2400){//stove temperature recording at 2Hz
    stoveRecordEnable = 1;     
   }
   if (tick==200){
      appUpdateEnable = 1;
   }         
//reason for ifs: timer interrupt does not work if too much things need to happen in interrupt. So on different ticks different things are done, BUT by setting flags and not actually doing anything.
}

void loop() { 
if(bluetoothSerial.available()){
    n = bluetoothSerial.readBytesUntil('$', incoming, 4); //0=automation, 1=speed, 2=maxspeed, 3=steppper, 4=reference temperature
    newUserInput();
}//end of bluetooth receive
   
if (updateFan==1){
  if((automation ==1)&&(Tk>50)){//checks if automation checked by user and if fireplace warm enough (automation ==1)&&(Tk>50)
     escGoal = round(computePI(Tavg));//input = Tavg, output = fanspeed
      escGoal = map(escGoal, 0, 100, 30, MAXSPEED);
  }     
      updateFan=0;    
}

updateStepperFlags();
   
if(roomRecordEnable==1){
  roomRecordTemps();
}
 if(stoveRecordEnable==1){//read stove temps at 1Hz
  Tk = thermocouple.readCelsius();
  stoveRecordEnable = 0;
}
    
if (dataLoggerUpdate == 1){
  dataLogger();
}
if (appUpdateEnable ==1){
  appUpdate();
}

esc.write(escGoal);//write pwm signal to esc
checkStepLimits();//check if lever reached limits and make changes if so
}

void stepperRangeFinder(){
  digitalWrite(dirPin, LOW);
  while(digitalRead(leftLimit) == LOW){
    digitalWrite(stepPin, HIGH);  
    delay(10);
    digitalWrite(stepPin, LOW);
    delay(10);  
  }
  digitalWrite(dirPin, HIGH);
  stepsPerStroke = 0;
  while(digitalRead(rightLimit)==LOW){
    digitalWrite(stepPin, HIGH);    
    delay(10);
    digitalWrite(stepPin, LOW);
    delay(10);
    stepsPerStroke++;
  }
  digitalWrite(dirPin, LOW);
  stepCurrent = stepsPerStroke;
  stepGoal = stepCurrent;
}
void timerSetup(){
  cli();//stop interrupts
//set timer2 interrupt at 8kHz
  TCCR2A = 0;// set entire TCCR2A register to 0
  TCCR2B = 0;// same for TCCR2B
  TCNT2  = 0;//initialize counter value to 0
  // set compare match register for 8khz increments
  OCR2A = 249;// = (16*10^6) / (8000*8) - 1 (must be <256)
  // turn on CTC mode
  TCCR2A |= (1 << WGM21);
  // Set CS21 bit for 8 prescaler
  TCCR2B |= (1 << CS21);   
  // enable timer compare interrupt
  TIMSK2 |= (1 << OCIE2A);
        sei();//allow interrupts
}
void newUserInput(){
   if(incoming[0]=='0'){//automation check
      if(incoming[1]=='1'){
        automation = 1; //enable automation
      }
      else{
        automation = 0; //disable automation
      }     
    }
    if(incoming[0]=='4'){//reference temperature
      tempRef = ((incoming[1]-48)*10)+incoming[2]-48; 
    } 
    if (automation == 0){
      doManualChanges();
    }
}
void updateStepperFlags(){
  if (dirState ==1){
      digitalWrite(dirPin, HIGH);
    }else{
      digitalWrite(dirPin, LOW);
    }
    if(stepState==1){     
             digitalWrite(stepPin,HIGH);
             digitalWrite(13, HIGH);            
    }
    if(stepState==0){
            digitalWrite(stepPin, LOW);
            digitalWrite(13, LOW);
    }
}
void doManualChanges(){
  if(incoming[0] == '1'){ //change fan speed
      //      kp = 2*(((incoming[1]-48)*10)+incoming[2]-48);
          escGoal = ((incoming[1]-48)*10)+incoming[2]-48;
          escGoal = map(escGoal, 0, 100, 30, 110);
          if(recordMaxSpeed == 1){ 
            MAXSPEED = escGoal;
          }
    }
    if(incoming[0] == '2'){ //max speed setup
      if(incoming[1] == '1'){
        recordMaxSpeed = 1;

      }
      else{
        recordMaxSpeed = 0;
      }      
    }
    if(incoming[0] == '3'){ //change stepper reference
          //kp = ((incoming[1]-48)*10)+incoming[2]-48;
          stepGoal = ((incoming[1]-48)*10)+incoming[2]-48;
          stepGoal = map(stepGoal, 0, 99, 0, stepsPerStroke);    
    }   
}

void checkStepLimits(){
  if(digitalRead(leftLimit) == 1){
    stepCurrent = 0;
  }
  if(digitalRead(rightLimit) ==1){
    stepCurrent = stepsPerStroke;
  }
}

void roomRecordTemps(){
  Vo1 = analogRead(ThermistorPin1);
    R2_1 = R1_1 * (1023.0 / (float)Vo1 - 1.0);
    logR2_1 = log(R2_1);
    T1 = (1.0 / (c1_1 + c2_1*logR2_1 + c3_1*logR2_1*logR2_1*logR2_1));
    T1 = 0.905*(T1 - 273.15)+2.431;   //calibration executed here
    
    Vo2 = analogRead(ThermistorPin2);
    R2_2 = R1_2 * (1023.0 / (float)Vo2 - 1.0);
    logR2_2 = log(R2_2);
    T2 = (1.0 / (c1_2 + c2_2*logR2_2 + c3_2*logR2_2*logR2_2*logR2_2));
    T2 = 0.906*(T2 - 273.15)+2.236; //calibration executed here
    Tavg = (T1+T2)/2;
    roomRecordEnable= 0;
}

void dataLogger(){
  roundTo2Dec();
  Serial.flush();
  Serial.print('$');
  Serial.print(t);
  Serial.print(',');
  Serial.print(T1avg);
  Serial.print(',');
  Serial.print(T2avg);
  Serial.print(',');
  Serial.print(Tk);
  Serial.print(',');
  Serial.print(tempRef);//round(100*stepCurrent/stepsPerStroke)
  Serial.print(',');
  Serial.print(round(100*(escGoal-30)/90));  
  dataLoggerUpdate = 0;
  //Serial.println();//just for serial monitor, not part of code
}
void roundTo2Dec(){
  T1avg = round(T1*100)/100;
  T2avg = round(T2*100)/100;
  Tk = round (Tk*100)/100;
}
void appUpdate(){   
    bluetoothSerial.flush();
    if (perfectTemp==1){
      bluetoothSerial.print(97);
      perfectTemp = 0;
    }
    else if((tooCold ==1)&&(coldSent==0)){
      bluetoothSerial.print(98);
      coldSent = 1;
    }
    else if((tooWarm ==1)&&(warmSent==0)){
      bluetoothSerial.print(99);
      warmSent = 1;
    }
    else {
      bluetoothSerial.print(Tavg); //or kp
    }
    appUpdateEnable=0;
}


double computePI(double inp){
  currentTime = millis();
  elapsedTime = (double)(currentTime - previousTime);
  errorNow = tempRef - inp;
  dataBlockA = kp*(errorNow - errorPrev);
  dataBlockB = ki*0.5*elapsedTime*(errorNow+errorPrev)/1000; //divide by 1000 because elapsed time in ms
  outNow = outPrev + dataBlockA + dataBlockB;
  outNow = constrain(outNow,0,100); 
  outPrev = outNow;
  errorPrev = errorNow;
  previousTime = currentTime;
  fireplaceStatus();
 
  return outNow;
}

void fireplaceStatus(){
   if (errorNow>0.5){
    c++;
  }else{
    if(tooCold ==1){      
     perfectTemp=1;
      tooCold = 0;
    }
    c=0;
  }
  if (errorNow<-0.5){
    w++;
  }else{
    if(tooWarm==1){
      tooWarm = 0;
      perfectTemp=1;
    }
    w=0;
  }
  if(c>=60){
    coldSent=0;
    tooCold = 1;
    c=0;
  }
  if(w>=60){
    warmSent=0;
    tooWarm = 1;
    w=0;
  } 
}
