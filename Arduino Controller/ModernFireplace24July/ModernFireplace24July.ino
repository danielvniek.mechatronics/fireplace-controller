#include <SoftwareSerial.h>
#include <Servo.h>
#include "max6675.h"
#include <Math.h>
#define TxD 11
#define RxD 10
SoftwareSerial bluetoothSerial(RxD,TxD);
char incoming[4];
char n;
int automation = 0;

//esc init start
Servo esc;
int escVal=30;
int escGoal=30;
int MAXSPEED=110;
int recordMaxSpeed = 0;
const int pwmPin = 12;
//esc init end
//thermoK init start
const int soPin=4;
const int csPin=5;
const int sckPin=6;
MAX6675 thermocouple(sckPin,csPin,soPin);
int stoveRecordEnable = 0;
float Tk;
//thermoK init end
//stepper init start
const int dirPin = 7;
const int stepPin = 8;
const int leftLimit = 2;
const int rightLimit = 3;
int tick = 0;
int controltick=0;
int mod = 1;
int stepsPerStroke = 0;
int stepGoal = 0;
int stepCurrent = 0;//change back to 0
int stepState = 0;
int dirState = 0;
//stepper init end
//thermistors init start
int roomRecordEnable=0;
int ThermistorPin1 = 0;
int Vo1;
float R1_1 = 10000;
float logR2_1, R2_1, T1;
float c1_1 = 1.009249522e-03, c2_1 = 2.378405444e-04, c3_1 = 2.019202697e-07;

int ThermistorPin2 = 1;
int Vo2;
float R1_2 = 10000;
float logR2_2, R2_2, T2;
float c1_2 = 1.009249522e-03, c2_2 = 2.378405444e-04, c3_2 = 2.019202697e-07;

float T1total=0.01;//prevent division by 0
float T2total=0.01;

int nTemps=0;
float T1avg=0;
float T2avg=0;

float Tavg=0;
//thermistors init end

int dataLoggerUpdate = 0;
float t = 0.00;
float O = 50.54;
float V = 15.23;
float checkSerial = 0.00;

int appUpdateEnable = 0;

//PID constants
double kp = 55;//15
double ki = 0;//5
double kd = 0;
 
unsigned long currentTime=0;
unsigned long previousTime=0;
double elapsedTime;
double cumError=0;
double input, output, setPoint;
  double outNow;
  double outPrev;
  double outPrevPrev;
  double errorNow;
  double errorPrev;
  double errorPrevPrev;
  double error3, error4, error5, error6, error7, error8, error9, error10;
  double dataBlockA;
  double dataBlockB;
  double dataBlockC;

int tempRef = 25;
int updateFan=0;
int controlLag = 0;

void setup() {
  bluetoothSerial.begin(9600);
  Serial.begin(9600);
  esc.attach(pwmPin, 1000, 2000);
  pinMode(dirPin, OUTPUT);
  pinMode(stepPin, OUTPUT);
  pinMode(leftLimit, INPUT);
  pinMode(rightLimit, INPUT);  


 //stepperRangeFinder(); //to find amount of steps from left side to right side: code will not continue until limit pins have gone high in the right order
 
 timerSetup();
  
  
  

}

ISR(TIMER2_COMPA_vect){  //stepping and temp recording timer at 8kHz
  tick++;
  
  mod = tick%100;
  
  
  if(mod == 0){
    if (stepCurrent > stepGoal){
             dirState = 0;
             stepState = 1;
             stepCurrent--; 
  }
   if (stepCurrent < stepGoal){
             dirState = 1;
             stepState = 1;
             stepCurrent++; 
  }
  }
     if(mod ==50){
            stepState = 0;
          
     }
     if (tick==4000){
      tick= 0;
      controltick++;
     }
     if (controltick ==4){
      controlLag = 1;
      controltick = 0;
     }
     if (tick%1000==0){  // take room temp samples at 8Hz:highest rate possible that allows all measurements to be taken without data corruption or timer delays
      roomRecordEnable = 1;
    
    }
    if (tick%2000==0){
       updateFan = 1;
    }
     
     if ((tick == 500)||(tick==2500)){//((tick == 500)||(tick==2500))&&(Tk!=0)&&(Tavg!=0)  not on thousands so that room record and gui update never on same instant. Update 4 times a second
      dataLoggerUpdate = 1;
      t= t+0.25;
     }
     if (tick ==2400){//stove temperature recording at 2Hz
      stoveRecordEnable = 1;
      
     }
     if (tick==200){
        appUpdateEnable = 1;
     }
      
     
//reason for ifs: timer interrupt does not work if too much things need to happen in interrupt. So on different ticks different things are done, BUT by setting flags and not actually doing anything.

   
 

}


void loop() {

  
  if(bluetoothSerial.available()){

    n = bluetoothSerial.readBytesUntil('$', incoming, 4); //0=automation, 1=speed, 2=maxspeed, 3=steppper, 4=temp

    if(incoming[0]=='0'){//automation check
      if(incoming[1]=='1'){
        automation = 1; //enable automation
      }
      else{
        automation = 0; //disable automation
      }     
    }
    if(incoming[0]=='4'){//reference temperature
      tempRef = ((incoming[1]-48)*10)+incoming[2]-48; 
    }
 
    if (automation == 0){
      doManualChanges();
    }

   
    
  }//end of bluetooth receive
//  if ((automation==1)&&(controlLag==1)){
//    
//          // if(Tk>40){//this part only for concept now since temp measurements from thermocouple way off.
//         escGoal = round(computePID2(Tavg));//input = Tavg, output = fanspeed
//        
//      escGoal = map(escGoal, 0, 100, 30, MAXSPEED);
//      escGoal = constrain(escGoal,30,MAXSPEED);
//       Serial.println(escGoal);
//      
//      controlLag = 0;
//        }     
if (updateFan==1){
  if(automation ==1){
     escGoal = round(computePID2(Tavg));//input = Tavg, output = fanspeed
      escGoal = constrain(escGoal,0,100); 
      escGoal = map(escGoal, 0, 100, 30, MAXSPEED);
     
      //Serial.println(escGoal);
  }
   
//  if((escGoal<70)&&(escVal<50)){
//    escVal = escGoal;
//  }
//  else if(escGoal>escVal+10){
//    escVal = escVal+10;
//  }
//  else if(escGoal<escVal-10){
//    escVal = escVal-10;
//  }else{
//    escVal = escGoal;
//  }
//    if((escGoal>80)&&(escVal>80)){
//      if(escGoal>escVal+5){
//        escVal = escVal + 5;
//      }
//      else if(escGoal<escVal-5){
//        escVal = escVal - 5;
//      }else{
//        escVal = escGoal;
//      }
//    }else{
//      if(escGoal>escVal+10){
//        escVal = escVal + 10;
//      }
//      else if(escGoal<escVal-10){
//        escVal = escVal - 10;
//      }
//     else{
//        escVal = escGoal;
//      }
//    }
//  Serial.println(escVal);
      
      updateFan=0;
     
    }
 
   if (dirState ==1){
      digitalWrite(dirPin, HIGH);
    }else{
      digitalWrite(dirPin, LOW);
    }
    if(stepState==1){     
             digitalWrite(stepPin,HIGH);
             digitalWrite(13, HIGH);
              
    }
    if(stepState==0){
            digitalWrite(stepPin, LOW);
            digitalWrite(13, LOW);
    }
    if(roomRecordEnable==1){
      roomRecordTemps();
    }
     if(stoveRecordEnable==1){//read stove temps at 1Hz
      Tk = thermocouple.readCelsius();
      stoveRecordEnable = 0;
    }
    
//    if(nTemps>=8){//take room temp avg after 100 samples @ 80Hz
//     
//      T1avg = T1total/nTemps;
//      T2avg = T2total/nTemps;
//
//      Tavg = (T1avg+T2avg)/2;
//      T1total = 0.01;//prevent division by 0
//      T2total = 0.01;
//
//      nTemps = 0;
//  }
  if (dataLoggerUpdate == 1){
    dataLogger();
  }
  if (appUpdateEnable ==1){
    appUpdate();
  }

   esc.write(escGoal);//update fan speed
  checkStepLimits();//check if lever reached limits and make changes if so
}

void stepperRangeFinder(){
  digitalWrite(dirPin, LOW);
  while(digitalRead(leftLimit) == LOW){
    digitalWrite(stepPin, HIGH);
  
    delay(10);
    digitalWrite(stepPin, LOW);
    delay(10);  
  }
  digitalWrite(dirPin, HIGH);
  stepsPerStroke = 0;
  while(digitalRead(rightLimit)==LOW){
    digitalWrite(stepPin, HIGH);
    
    delay(10);
    digitalWrite(stepPin, LOW);
    delay(10);
    stepsPerStroke++;
  }
  digitalWrite(dirPin, LOW);
  stepCurrent = stepsPerStroke;
  stepGoal = stepCurrent;
}
void timerSetup(){
  cli();//stop interrupts
//set timer2 interrupt at 8kHz
  TCCR2A = 0;// set entire TCCR2A register to 0
  TCCR2B = 0;// same for TCCR2B
  TCNT2  = 0;//initialize counter value to 0
  // set compare match register for 8khz increments
  OCR2A = 249;// = (16*10^6) / (8000*8) - 1 (must be <256)
  // turn on CTC mode
  TCCR2A |= (1 << WGM21);
  // Set CS21 bit for 8 prescaler
  TCCR2B |= (1 << CS21);   
  // enable timer compare interrupt
  TIMSK2 |= (1 << OCIE2A);
        sei();//allow interrupts
}

void doManualChanges(){
  if(incoming[0] == '1'){ //change fan speed
            kp = 2*(((incoming[1]-48)*10)+incoming[2]-48);
//          escGoal = ((incoming[1]-48)*10)+incoming[2]-48;
//          escGoal = map(escGoal, 0, 100, 30, 110);
          if(recordMaxSpeed == 1){ 
            MAXSPEED = escGoal;
          }
    }
    if(incoming[0] == '2'){ //max speed setup
      if(incoming[1] == '1'){
        recordMaxSpeed = 1;

      }
      else{
        recordMaxSpeed = 0;
      }      
    }

    if(incoming[0] == '3'){ //change stepper reference
        ki = ((incoming[1]-48)*10)+incoming[2]-48;
//          stepGoal = ((incoming[1]-48)*10)+incoming[2]-48;
//          stepGoal = map(stepGoal, 0, 99, 0, stepsPerStroke);
    
    }   
}
void checkStepLimits(){
  if(digitalRead(leftLimit) == 1){
    stepCurrent = 0;
  }
  if(digitalRead(rightLimit) ==1){
    stepCurrent = stepsPerStroke;
  }
}

void roomRecordTemps(){
  Vo1 = analogRead(ThermistorPin1);
    R2_1 = R1_1 * (1023.0 / (float)Vo1 - 1.0);
    logR2_1 = log(R2_1);
    T1 = (1.0 / (c1_1 + c2_1*logR2_1 + c3_1*logR2_1*logR2_1*logR2_1));
    T1 = 0.905*(T1 - 273.15)+2.431;   //calibration executed here


    Vo2 = analogRead(ThermistorPin2);
    R2_2 = R1_2 * (1023.0 / (float)Vo2 - 1.0);
    logR2_2 = log(R2_2);
    T2 = (1.0 / (c1_2 + c2_2*logR2_2 + c3_2*logR2_2*logR2_2*logR2_2));
    T2 = 0.906*(T2 - 273.15)+2.236; //calibration executed here
    Tavg = (T1+T2)/2;
    roomRecordEnable= 0;
}

void dataLogger(){
  roundTo2Dec();
  Serial.flush();
  Serial.print('$');
  Serial.print(t);
  Serial.print(',');
  Serial.print(T1avg);
  Serial.print(',');
  Serial.print(T2avg);
  Serial.print(',');
  Serial.print(Tk);
  Serial.print(',');
  Serial.print(round(stepCurrent/stepsPerStroke));//round(100*stepCurrent/stepsPerStroke)
  Serial.print(',');
  Serial.print(round(100*(escGoal-30)/90));

  
  dataLoggerUpdate = 0;
  //Serial.println();//just for serial monitor, not part of code
}
void roundTo2Dec(){
  T1avg = round(T1*100)/100;
  T2avg = round(T2*100)/100;
  Tk = round (Tk*100)/100;
}
void appUpdate(){

  
    bluetoothSerial.flush();
    bluetoothSerial.print(Tavg);

    appUpdateEnable=0;

}

double ownPID(double inp){
  errorNow = tempRef - inp;
  currentTime = millis();
  elapsedTime = (double)(currentTime - previousTime)/1000;
  cumError += errorNow * elapsedTime;
  outNow = kp*errorNow+ki*cumError;
  previousTime = currentTime;
  return outNow;
}

double computePID2(double inp){

  currentTime = millis();
  elapsedTime = (double)(currentTime - previousTime);
  errorNow = tempRef - inp;
  dataBlockA = kp*(errorNow - error5);
  dataBlockB = ki*0.5*elapsedTime*(errorNow+2*errorPrev+errorPrevPrev+error3+error4+error5+error6+error7+error8+error9+error10)/10000;
  dataBlockC = kd*(2/elapsedTime)*(errorNow-2*errorPrev+errorPrevPrev);
  outNow = outPrevPrev + dataBlockA + dataBlockB + dataBlockC;


  outPrevPrev = outPrev;
  outPrev = outNow;
  error10=error9;
  error9=error8;
  error8=error7;
  error7=error6;
  error6=error5;
  error5=error4;
  error4=error3;
  error3=errorPrevPrev;
  errorPrevPrev=errorPrev;
  errorPrev = errorNow;
  previousTime = currentTime;
 // Serial.println(outNow);
  return outNow;
  
  
  
}

//PID added, but not tested yet
