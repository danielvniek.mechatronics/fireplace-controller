#include <SoftwareSerial.h>
#include <Servo.h>
#include "max6675.h"
#include <Math.h>
#define TxD 11
#define RxD 10
SoftwareSerial bluetoothSerial(RxD,TxD);
char incoming[4];
char n;
int automation = 0;

//esc init start
Servo esc;
int escVal;
int MAXSPEED=0;
int recordMaxSpeed = 0;
const int pwmPin = 12;
//esc init end
//thermoK init start
const int soPin=4;
const int csPin=5;
const int sckPin=6;
MAX6675 thermocouple(sckPin,csPin,soPin);
int stoveRecordEnable = 0;
float Tk;
//thermoK init end
//stepper init start
const int dirPin = 7;
const int stepPin = 8;
const int leftLimit = 2;
const int rightLimit = 3;
int tick = 0;
int mod = 1;
int stepsPerStroke = 20;
int stepGoal = 0;
int stepCurrent = 0;//change back to 0
int stepState = 0;
int dirState = 0;
//stepper init end
//thermistors init start
int roomRecordEnable=0;
int ThermistorPin1 = 0;
int Vo1;
float R1_1 = 10000;
float logR2_1, R2_1, T1;
float c1_1 = 1.009249522e-03, c2_1 = 2.378405444e-04, c3_1 = 2.019202697e-07;

int ThermistorPin2 = 1;
int Vo2;
float R1_2 = 10000;
float logR2_2, R2_2, T2;
float c1_2 = 1.009249522e-03, c2_2 = 2.378405444e-04, c3_2 = 2.019202697e-07;

float T1total=0.01;//prevent division by 0
float T2total=0.01;

int nTemps=0;
float T1avg=0;
float T2avg=0;

float Tavg=0;
//thermistors init end

int guiUpdateEnable = 0;
float t = 0.00;
float O = 50.54;
float V = 15.23;
float checkSerial = 0.00;

int appUpdateEnable = 0;

//PID constants
double kp = 2;
double ki = 5;
double kd = 1;
 
unsigned long currentTime, previousTime;
double elapsedTime;
double error;
double lastError;
double input, output, setPoint;
double cumError, rateError;

int tempRef = 25;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
Vo1 = analogRead(0);
    R2_1 = R1_1 * (1023.0 / (float)Vo1 - 1.0);
    logR2_1 = log(R2_1);
    T1 = (1.0 / (c1_1 + c2_1*logR2_1 + c3_1*logR2_1*logR2_1*logR2_1));
    T1avg = T1 - 273.15;
    T1total = T1total +T1;

    Vo2 = analogRead(ThermistorPin2);
    R2_2 = R1_2 * (1023.0 / (float)Vo2 - 1.0);
    logR2_2 = log(R2_2);
    T2 = (1.0 / (c1_2 + c2_2*logR2_2 + c3_2*logR2_2*logR2_2*logR2_2));
    T2avg = T2 - 273.15;
    T2total = T2total+T2;
    nTemps++;
    roomRecordEnable= 0;

    Tk = thermocouple.readCelsius();

    T1avg = round(T1avg*100)/100;
  T2avg = round(T2avg*100)/100;
  Tk = round (Tk*100)/100;
  delay(200);
  Serial.println(T1avg);
  //Serial.println(T2avg);
  //Serial.println(Tk);
  
}
