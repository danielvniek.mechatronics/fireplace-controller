

int ThermistorPin1 = 0;
int Vo1;
float R1_1 = 10000;
float logR2_1, R2_1, T1;
float c1_1 = 1.009249522e-03, c2_1 = 2.378405444e-04, c3_1 = 2.019202697e-07;

int ThermistorPin2 = 1;
int Vo2;
float R1_2 = 10000;
float logR2_2, R2_2, T2;
float c1_2 = 1.009249522e-03, c2_2 = 2.378405444e-04, c3_2 = 2.019202697e-07;

void setup() {
  Serial.begin(9600);
}

void loop() {
  roomRecordTemps;
  delay(500);
  Serial.println(T1);
  Serial.println(T2);
  Serial.println(',');
}

void roomRecordTemps(){
  Vo1 = analogRead(ThermistorPin1);
    R2_1 = R1_1 * (1023.0 / (float)Vo1 - 1.0);
    logR2_1 = log(R2_1);
    T1 = (1.0 / (c1_1 + c2_1*logR2_1 + c3_1*logR2_1*logR2_1*logR2_1));
    T1 = 0.905*(T1 - 273.15)+2.431;   //calibration executed here

    Vo2 = analogRead(ThermistorPin2);
    R2_2 = R1_2 * (1023.0 / (float)Vo2 - 1.0);
    logR2_2 = log(R2_2);
    T2 = (1.0 / (c1_2 + c2_2*logR2_2 + c3_2*logR2_2*logR2_2*logR2_2));
    T2 = 0.906*(T2 - 273.15)+2.236; //calibration executed here
}
