

import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure

from tkinter import *
import pandas as pd



t = []
T1 = []
T2 = []
Tf = []
O = []
V = []
Wood = []
tnew=0.0
T1new=0.0
T2new = 0.0
Tfnew = 0.0
Onew = 0.0
Vnew = 0.0
Wnew = 0




def updatePlots():
    a1.clear
    a1.plot(t,T1, marker = 'o', markersize = 2, color = 'blue')
    canvas1.draw()
    a2.clear
    a2.plot(t,T2, marker = 'o', markersize = 2, color = 'blue')
    canvas2.draw()
    a3.clear
    a3.plot(t,Tf, marker = 'o', markersize = 2, color = 'blue')
    canvas3.draw()
    a4.clear
    a4.plot(t,O, marker = 'o', markersize = 2, color = 'blue')
    canvas4.draw()
    a5.clear
    a5.plot(t,V, marker = 'o', markersize = 2, color = 'blue')
    canvas5.draw()   

def woodPutIn():
    global Wnew
    Wnew = 1
    


win = Tk()
autoChecked = IntVar(win)
tVar = StringVar(win)
T1Var = StringVar(win)
T2Var = StringVar(win)
TfVar = StringVar(win)
OVar = StringVar(win)
VVar = StringVar(win)

TopFrame = Frame(win)
TopFrame.pack(side=TOP, fill = X)
BottomFrame = Frame(win)
BottomFrame.pack(side = TOP, fill = X)

TopLFrame = Frame(TopFrame)
TopLFrame.pack(side=LEFT)
f1 = Figure(figsize=(5,3.5),dpi=100)
f1.suptitle("Temperature 1 vs time")
a1 = f1.add_subplot(111)
a1.plot(t, T1, marker = 'o')
canvas1 = FigureCanvasTkAgg(f1,master = TopLFrame)
canvas1.draw()
canvas1.get_tk_widget().pack(side=TOP)
toolbar1 = NavigationToolbar2Tk(canvas1, TopLFrame)
toolbar1.update()
canvas1._tkcanvas.pack(side=TOP)

TopMFrame = Frame(TopFrame)
TopMFrame.pack(side=LEFT)
f2 = Figure(figsize=(5,3.5),dpi=100)
f2.suptitle("Temperature 2 vs time")
a2 = f2.add_subplot(111)
a2.plot(t,T2, marker = 'o')
canvas2 = FigureCanvasTkAgg(f2,master = TopMFrame)
canvas2.draw()
canvas2.get_tk_widget().pack(side=TOP)
toolbar2 = NavigationToolbar2Tk(canvas2, TopMFrame)
toolbar2.update()
canvas2._tkcanvas.pack(side=TOP)

TopRFrame = Frame(TopFrame)
TopRFrame.pack(side=LEFT)
f3 = Figure(figsize=(5,3.5),dpi=100)
f3.suptitle("Fireplace temperature vs time")
a3 = f3.add_subplot(111)
a3.plot(t,Tf, marker = 'o')
canvas3 = FigureCanvasTkAgg(f3,master = TopRFrame)
canvas3.draw()
canvas3.get_tk_widget().pack(side=TOP)
toolbar3 = NavigationToolbar2Tk(canvas3, TopRFrame)
toolbar3.update()
canvas3._tkcanvas.pack(side=TOP)

BottomLFrame = Frame(BottomFrame)
BottomLFrame.pack(side=LEFT)        
f4 = Figure(figsize=(5,3.5),dpi=100)
f4.suptitle("Oxygen intake % vs time")
a4 = f4.add_subplot(111)
a4.plot(t,O, marker = 'o')
canvas4 = FigureCanvasTkAgg(f4,master = BottomLFrame)
canvas4.draw()
canvas4.get_tk_widget().pack(side=TOP)
toolbar4 = NavigationToolbar2Tk(canvas4, BottomLFrame)
toolbar4.update()
canvas4._tkcanvas.pack(side=TOP)

BottomMFrame = Frame(BottomFrame)
BottomMFrame.pack(side=LEFT)
f5 = Figure(figsize=(5,3.5),dpi=100)
f5.suptitle("Fan speed (rpm) vs time")
a5 = f5.add_subplot(111)
a5.plot(t,V, marker = 'o')
canvas5 = FigureCanvasTkAgg(f5,master = BottomMFrame)
canvas5.draw()
canvas5.get_tk_widget().pack(side=TOP)
toolbar5 = NavigationToolbar2Tk(canvas5, BottomMFrame)
toolbar5.update()
canvas5._tkcanvas.pack(side=TOP)

BottomRFrame = Frame(BottomFrame)
BottomRFrame.pack(side=LEFT)
updateBtn = Button(BottomRFrame, text = "Update")
updateBtn.pack(side=TOP)
updateBtn.config(command = updatePlots)
#autoUpdate = Checkbutton(BottomRFrame, text = "Auto-Update", variable = autoChecked).pack(side=TOP)
woodInput = Button(BottomRFrame, text = "Wood put in")
woodInput.pack(side=TOP)
woodInput.config(command = woodPutIn)
tlabel = Label(BottomRFrame, textvariable = tVar).pack(side=TOP)
T1label = Label(BottomRFrame, textvariable = T1Var).pack(side=TOP)
T2label = Label(BottomRFrame, textvariable = T2Var).pack(side=TOP)
Tflabel = Label(BottomRFrame, textvariable = TfVar).pack(side=TOP)
Olabel = Label(BottomRFrame, textvariable = OVar).pack(side=TOP)
Vlabel = Label(BottomRFrame, textvariable = VVar).pack(side=TOP)





import serial
import threading
import time


def threadser():#expect t,T1,T2,Tf,O,V, all two digits
    global tnew
    global T1new
    global T2new
    global Tfnew
    global Onew
    global Vnew
    global Wnew

    while True:
    	data = arduino.readline() 
    	if data:
             start = time.time()
             if (data[0]==36):
                 i = len(data)-1
                 commaCheck=0
                 while(data[i]!=36):
                     i=i-1
                     if (data[i]==44):
                         commaCheck=commaCheck+1
                 if (commaCheck==5):
                     i = len(data)-1
                     pw=-2
                     Vnew = 0
                     while(data[i]!=44):
                         if (data[i] != 46):
                             Vnew = Vnew + (data[i]-48)*(10**pw)
                             pw=pw+1
                         i=i-1
                     i = i-1
                     pw=-2
                     Onew = 0
                     while(data[i]!=44):
                         if (data[i] != 46):
                             Onew = Onew + (data[i]-48)*(10**pw)
                             pw=pw+1
                         i=i-1
                     i = i-1
                     pw=-2
                     Tfnew = 0
                     while(data[i]!=44):
                         if (data[i] != 46):
                             Tfnew = Tfnew + (data[i]-48)*(10**pw)
                             pw=pw+1
                         i=i-1
                     i = i-1
                     pw=-2
                     T2new = 0
                     while(data[i]!=44):
                         if (data[i] != 46):
                             T2new = T2new + (data[i]-48)*(10**pw)
                             pw=pw+1
                         i=i-1
                     i = i-1
                     pw=-2
                     T1new = 0
                     while(data[i]!=44):
                         if (data[i] != 46):
                             T1new = T1new + (data[i]-48)*(10**pw)
                             pw=pw+1
                         i=i-1
                     i = i-1
                     pw=-2
                     tnew = 0
                     while(data[i]!=36):
                         if (data[i] != 46):
                             tnew = tnew + (data[i]-48)*(10**pw)
                             pw=pw+1
                         i=i-1
                         
                     t.append(tnew)
                     T1.append(T1new)
                     T2.append(T2new)
                     Tf.append(Tfnew)
                     O.append(Onew)
                     V.append(Vnew)
                             
                     writer = pd.ExcelWriter('FireplaceData.xlsx', engine='xlsxwriter')
                     df1 = pd.DataFrame({'time': t})
                     df2 = pd.DataFrame({'T1': T1})
                     df3 = pd.DataFrame({'T2': T2})
                     df4 = pd.DataFrame({'Tf': Tf})
                     df5 = pd.DataFrame({'O': O})
                     df6 = pd.DataFrame({'V': V})
                     
                     
                     df1.to_excel(writer, sheet_name='Sheet1',startcol = 0)
                     df2.to_excel(writer, sheet_name='Sheet1', startcol = 2)
                     df3.to_excel(writer, sheet_name='Sheet1', startcol = 4)
                     df4.to_excel(writer, sheet_name='Sheet1', startcol = 6)
                     df5.to_excel(writer, sheet_name='Sheet1', startcol = 8)
                     df6.to_excel(writer, sheet_name='Sheet1', startcol = 10)
                     df7 = pd.DataFrame({'Wood input times': Wood})
                     df7.to_excel(writer, sheet_name='Sheet1',startcol = 12, startrow = 0)
                     if (Wnew == 1):
                         print("wood")
                         Wood.append(tnew)
                         df7 = pd.DataFrame({'Wood input times': Wood})
                         df7.to_excel(writer, sheet_name='Sheet1',startcol = 12, startrow = 0)
                         Wnew = 0
                     writer.save()
                     
                     tVar.set("t = %.2f" %tnew)
                     T1Var.set("T1 = %.2f" %T1new)
                     T2Var.set("T2 = %.2f" %T2new)
                     TfVar.set("Tf = %.2f" %Tfnew)
                     OVar.set("O = %.2f" %Onew)
                     VVar.set("V = %.2f" %Vnew)
                     end = time.time()
                     print(end-start)
             

             

            
             
arduino = serial.Serial('COM3', 9600, timeout=.1)

a = threading.Thread(target = threadser,name='Thread-a',daemon=True)
a.start()   
win.mainloop()