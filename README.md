_Note that this README serves as a mere overview of the project and more in depth documentation can be found in doc/Fireplace Controller.pdf_
# Description #
The objective of this project was to design and implement a concept that will allow an existing fireplace to distribute its heat evenly in a room and control the room's temperature. This was done by adding actuators that could open and close the fireplace's oxygen intake levers, as well as a fan that could push wind past the fireplace into the room at varying velocities. 
The actuators and fan was controlled by an Arduino Microcontroller which received temperature feedback from three places in the room and from a thermocouple inside the fireplace. The figure below shows the final setup.\
<img src="/doc/RM/RM1.JPG" height="200">
## Air inlet actuator ##
To control the air inlet a stepper motor with a toothed pulley on its shaft driving a belt was used to open and close an air inlet lever as shown in the figure below. Limit switches were used to let the controller know when the lever has reached the left or right limit.\
<img src="/doc/RM/RM2.JPG" height="200"> \
This air inlet actuator was mounted on the fireplace in a way that would isolote the stepper motor from the fireplace's heat. The mount is shown below. \
<img src="/doc/RM/RM3.JPG" height="200"> \
## Fan ##
The following table shows which parts were used for the fan. A fan with a high variability in speed was required, which is why a quadcopter fan was used.\
| Part                                     | Component Selection                        |
|------------------------------------------|--------------------------------------------|
| Motor                                    | MT3506 - KV650                             | 
| Motor electronic  speed controller (ESC) | Hobbywing Skywalker 2-6s 60A Brushless ESC |
| Fan Blade                                | APC Multi-Rotor propellor 12x4.5 MRP       |
## Arduino Controller ##
The PI controller code used is shown below:
```
double ownPID(double inp){
  errorNow = tempRef - inp;
  currentTime = millis();
  elapsedTime = (double)(currentTime - previousTime)/1000;
  cumError += errorNow * elapsedTime;
  outNow = kp*errorNow+ki*cumError;
  previousTime = currentTime;
  return outNow;
}
```
The following figure shows an overview of the main loop in the Arduino Controller \
<img src="/doc/RM/RM4.JPG" height="800"> \
The different flags used in the controller's code, the frequency of these flags and their purposes are summorised in the table below\
| Flag              | Frequency | Flag purpose                                                                                                                                                                                                                                                                                                                                  |
|-------------------|-----------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| roomRecordEnable  | 8 Hz      | When this flag is set the main loop calls a function that records the temperatures measured by the two thermistors in the room.                                                                                                                                                                                                               |
| updateFan         | 4 Hz      | The frequency of this flag determines the rate at which the control system samples feedback and updates the fan speed. This flag is only set when the fireplace temperature is above 100 ℃.                                                                                                                                                   |
| dataLoggerUpdate  | 4 Hz      | When this flag is set, all in the inputs and outputs are saved in some manner. In this case it is saved by sending it over a UART interface to a PC, which saves it to excel through a Python program as discussed in the section 6.4.                                                                                                        |
| stoveRecordEnable | 2 Hz      | When this flag is set, the controller samples the temperature of the thermocouple inside the fireplace.                                                                                                                                                                                                                                       |
| appUpdateEnable   | 2 Hz      | When this flag is set, the controller uses the Bluetooth interface to update the average room temperature value displayed on the user’s phone. The controller also checks if the fireplace is too warm or too cold for the reference room temperature. Based on this, it updates the ‘Fireplace temperature’ window in the phone application. |\
## Python data logger ##
The table below shows the requirements of the data logger, as well as how each requirement was met using some python library.
| Requirement                                                                                                                                    | Library used | Library description                                                                                                                                                                                                                                                       |
|------------------------------------------------------------------------------------------------------------------------------------------------|--------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1. The program needs some sort of user interface.                                                                                              | Tkinter      | Tkinter is a library used to create a graphical user interface. This library was used to create all the buttons and displays the designer needs in the desired layout.                                                                                                    |
| 2. The program needs a way to read the incoming serial data.                                                                                   | Serial       | Serial allows the program to open a serial port and set the baud rate to the required 9600 bits/second. The library has a built-in function ‘readline()’, which reads characters into a character array until an end of line character is received.                       |
| 3. The program needs a way to plot the incoming data, to give the designer a way of monitoring the system’s inputs and outputs during testing. | Matplotlib   | This library makes it possible to embed plots into Python applications. In this application the incoming data is plotted as markers on a graph with lines connecting the markers. The graphs also have buttons that allows the designer to zoom in and out of the graphs. |
| 4. The program needs a way to store the incoming data.                                                                                         | Openpyxl     | This library allows the program to access excel workbooks to read and write data in the workbook. Each piece of data is saved to a single cell in Excel. The cell numbers are increased for each new set of data to populate the workbook.                                |
| 5. The program needs a way to continuously wait for incoming serial data, without freezing the Tkinter GUI.                                    | Threading    | Threading is a library that allows the program to execute two pieces of code simultaneously. For this program the GUI is run on one thread, while the function reading in new serial data is run on a second thread.                                                      | 


The figure below shows a screenshot of the python data logger. \
<img src="/doc/RM/RM5.JPG" height="600">  \
## Phone application ##
A phone application was developed using MIT app inventor. This application was used to control the system and see live data. The phone application communicated with the Arduino controller using Bluetooth. This application also represents a first draft of a phone application that might be used in an industrial version of this system. The figure below shows a screen shot of the application. \
<img src="/doc/RM/RM6.JPG" height="500">  \
## Wiring ##
The figure below shows the final wiring diagram of the system. \
<img src="/doc/RM/wiring.JPG" height="600">  \
## Simulations ##
Simulations were used to evaluate the validity of the project before starting it. The figure below shows the Simulink block diagrams used to model the room with the fireplace and the fan. \
<img src="/doc/RM/simulink.JPG" height="500">  \
# Results #
The figure below shows the performance of the system. In this figure it can be seen that the room temperure was below 17 degrees Celsius before the system was switched on, and took about 15 seconds to increase the average room temperature to 19 degrees Celcius. \
<img src="/doc/RM/RM7.JPG" height="400">  \
Two temperature sensors were used in the room to calculate an average room temperature. One sensor was closer to the fireplace than the other one. The figure below shows how the temperature was distributed in the room when for different reference temperatures. \
<img src="/doc/RM/RM8.JPG" height="400">  \


