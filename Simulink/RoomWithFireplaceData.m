roomWidth = 6;      %in m
roomLength = 3;
roomHeight = 3;
windowWidth = 3;
windowHeight = 1.2;

windowThickness = 0.01;
wallThickness = 0.2;
roofThickness = 0.2;       
floorThickness = 0.2;

roofArea = roomWidth*roomLength;
windowArea = windowWidth*windowHeight;
wallArea = 2*roomWidth*roomHeight + 2*roomLength*roomHeight - windowArea;
floorArea = roofArea;

roofDensity = 32;
floorDensity = 32;
wallDensity = 1920;
windowDensity = 2700;

roofMass = roofDensity*roofArea*roofThickness;
wallMass = wallDensity*wallArea*wallThickness;
windowMass = windowDensity*windowArea*windowThickness;
floorMass = floorDensity*floorArea*floorThickness;

P = 101325;     %in Pa
R = 287;        %SI units
T = 273 + 25;
airDensity = P/(R*T);   
airMass = airDensity*roomWidth*roomLength*roomHeight;

cAir = 1007;
cRoof = 835;
cFloor = 835;
cWall = 835;
cWindow = 840;

hAirRoof = 12;
hAirFloor = 12;
hAirWall = 24;
hAirWindow = 25;
hRoofAtm = 38;
hFloorGround = 38;  %assume same as roof for now, and that ground = atmosphere for now
hWallAtm = 34;
hWindowAtm = 32;

kRoof = 0.038;
kFloor = 0.038;
kWall = 0.038;
kWindow = 0.78;

TinsideIC = 10; %degC
ToutsideIC = 10; %degC

%%StoveData%%

stoveInnerArea = 0.4*0.3*4 + 0.2*0.3*2;
stoveOuterArea = 0.5*0.4*4 + 0.3*0.4*2;

stoveThickness = 0.02; %estimate
stoveMass = 80; %estimate;

hStoveInner = 50; %just values, no calculations yet
hStoveRoom = 60; %just values, no calculations yet

kStove = 180; %from textbook example
cStove = 880; %from textbook example








